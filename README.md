Versioning should be according to [Semantic Versioning](http://semver.org).

To use in your project:

```bash
composer require slyfoxcreative/accpac
```

Publish the Laravel configuration file to `config/accpac.php`:

```bash
$ php artisan vendor:publish --provider="SlyFoxCreative\Accpac\ServiceProvider"
```

Use via the Laravel facade:

```php
use SlyFoxCreative\Accpac\Facades\Accpac;

$order = Accpac::query('oeordh')->eq('ordnumber', '0177380')->first();
```

# Examples

How to get an order:

```php
use SlyFoxCreative\Accpac\Session;

$session = new Session(
    'hostname',
    'username',
    'password',
    'eelsbx',
    ['oeordh', 'oeordd'],
);

$order = $session
    ->query('oeordh')
    ->eq('ordnumber', '~TEST2~')
    ->select('customer', 'orddate', 'orduniq')
    ->first()
;

echo "$order->customer\n"; // TTN001
echo "$order->orddate\n"; // 2021-12-07 00:00:00

$items = $session
    ->query('oeordd')
    ->eq('orduniq', $order->orduniq)
    ->select('item', 'extweight')
    ->get()
;

foreach ($items as $item) {
    echo "$item->item\n" // MIS*TEST1
}

// Get the total weight for the order.
echo $items->map(fn ($i) => $i->extweight)->sum() . "\n"; // 80.0
```
