<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

/**
 * Provides a fluent API for building SELECT a query.
 */
class QueryBuilder
{
    private readonly string $table;

    /** @var Collection<int, string> */
    private Collection $selects;

    private ParameterList $params;

    private ConditionBuilder $conditions;

    private ?string $orderBy;

    private ?Direction $direction;

    private ?int $perPage;

    private ?int $offset;

    public function __construct(private readonly Session $session, string $table)
    {
        $this->table = $this->session->schema()->normalizeTableName($table);
        $this->selects = new Collection();
        $this->params = new ParameterList($this->session->schema());
        $this->conditions = new ConditionBuilder(
            $this,
            $this->session->schema(),
            $this->table,
            $this->params,
        );
    }

    /**
     * Add fields to the SELECT clause.
     */
    public function select(string ...$fields): static
    {
        $this->selects = $this->selects->concat($fields);

        return $this;
    }

    /**
     * Add an '=' condition to the WHERE clause.
     */
    public function eq(string $field, float|int|string|Carbon $value): static
    {
        $this->conditions->eq($field, $value);

        return $this;
    }

    /**
     * Add a '<>' condition to the WHERE clause.
     */
    public function ne(string $field, float|int|string|Carbon $value): static
    {
        $this->conditions->ne($field, $value);

        return $this;
    }

    /**
     * Add an 'IN' condition to the WHERE clause.
     *
     * @param  array<int, QueryData>|Collection<int, QueryData>  $values
     */
    public function in(string $field, array|Collection $values): static
    {
        if (is_array($values)) {
            $values = collect($values);
        }

        $this->conditions->in($field, $values);

        return $this;
    }

    /**
     * Add a 'LIKE' condition to the WHERE clause.
     */
    public function like(string $field, string $value): static
    {
        $this->conditions->like($field, $value);

        return $this;
    }

    /**
     * Add a '>' condition to the WHERE clause.
     */
    public function gt(string $field, float|int|string|Carbon $value): static
    {
        $this->conditions->gt($field, $value);

        return $this;
    }

    /**
     * Add a '>=' condition to the WHERE clause.
     */
    public function ge(string $field, float|int|string|Carbon $value): static
    {
        $this->conditions->ge($field, $value);

        return $this;
    }

    /**
     * Add a '<' condition to the WHERE clause.
     */
    public function lt(string $field, float|int|string|Carbon $value): static
    {
        $this->conditions->lt($field, $value);

        return $this;
    }

    /**
     * Add a '<=' condition to the WHERE clause.
     */
    public function le(string $field, float|int|string|Carbon $value): static
    {
        $this->conditions->le($field, $value);

        return $this;
    }

    /**
     * Add an IS NULL condition to the where clause.
     */
    public function null(string $field): static
    {
        $this->conditions->null($field);

        return $this;
    }

    /**
     * Negate the next condition.
     */
    public function not(): ConstrainedConditionBuilder
    {
        return $this->conditions->not();
    }

    /**
     * Add the next condition with an OR conjunction.
     */
    public function or(): ConstrainedConditionBuilder
    {
        return $this->conditions->or();
    }

    /**
     * Add a parenthesized grouping of conditions.
     */
    public function paren(callable $conditions): static
    {
        $this->conditions->paren($conditions);

        return $this;
    }

    /**
     * Add an ORDER BY constraint.
     */
    public function orderBy(string $field, Direction $direction = Direction::Ascending): static
    {
        $this->orderBy = $this->normalize($field);
        $this->direction = $direction;

        return $this;
    }

    /**
     * Add pagination constraints.
     *
     * @param  int  $page  Current page number
     * @param  int  $perPage  Number of rows per page
     */
    public function paginate(int $page = 1, int $perPage = 10): static
    {
        if ($page < 1) {
            throw new \ValueError('Page number must be >= 1');
        }
        if ($perPage < 1) {
            throw new \ValueError('Per-page count must be >= 1');
        }

        $this->perPage = $perPage;
        $this->offset = ($page - 1) * $perPage;

        return $this;
    }

    /**
     * Run the query and return the results as a Collection of DataObjects.
     *
     * @return Collection<int, DataObject>
     */
    public function getBase(): Collection
    {
        return $this->session
            ->fetchRows($this->sql(), $this->params())
            ->map(fn($row) => $this->session->schema()->convertValues($row, $this->table))
            ->map(fn($row) => new DataObject($this->table, $row))
        ;
    }

    /**
     * Run the query and return the first result as a DataObject, or null if
     * there are no results.
     */
    public function first(): ?DataObject
    {
        return $this->getBase()->first();
    }

    /**
     * Select the number of rows in the result.
     */
    public function count(): int
    {
        $sql = collect(["SELECT COUNT(*) FROM {$this->table}"])
            ->concat($this->constraints())
            ->join(' ')
        ;

        $value = $this->session->fetchValue($sql, $this->params());

        if (! is_int($value)) {
            throw new UnexpectedType($value, 'int');
        }

        return $value;
    }

    /**
     * Return the SQL query string.
     */
    public function sql(): string
    {
        $selects = $this->selects();

        $sql = collect(["SELECT {$selects} FROM {$this->table}"])
            ->concat($this->constraints())
            ->join(' ')
        ;

        return Str::squish($sql);
    }

    /**
     * Return the Collection of parameters keyed by placeholder name.
     *
     * @return Collection<string, AccpacData>
     */
    public function params(): Collection
    {
        return $this->params->get();
    }

    /**
     * Return the SELECT clause.
     */
    private function selects(): string
    {
        if ($this->selects->isEmpty()) {
            return '*';
        }

        return $this
            ->selects
            ->map(fn($f) => $this->normalize($f))
            ->join(', ')
        ;
    }

    /**
     * Return the WHERE, ORDER BY and pagination constraints.
     *
     * @return Collection<int, string>
     */
    private function constraints(): Collection
    {
        $constraints = new Collection();

        $conditions = "{$this->conditions}";
        if (($conditions) != '') {
            $constraints->push("WHERE {$conditions}");
        }

        if (isset($this->orderBy, $this->direction)) {
            $constraints->push("ORDER BY {$this->orderBy} {$this->direction->value}");
        }

        if (isset($this->perPage)) {
            $constraints->push("OFFSET {$this->offset} ROWS FETCH NEXT {$this->perPage} ROWS ONLY");
        }

        return $constraints;
    }

    /**
     * Normalize a field name and escape it with '[]'.
     */
    private function normalize(string $field): string
    {
        $s = $this->session->schema()->normalizeFieldName($this->table, $field);

        return "[{$s}]";
    }
}
