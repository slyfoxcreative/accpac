<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

class UnexpectedType extends \Exception
{
    public function __construct(mixed $value, string $expected)
    {
        $actual = is_object($value) ? get_class($value) : gettype($value);
        parent::__construct("Expected value of type {$expected}, got {$actual}");
    }
}
