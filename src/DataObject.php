<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class DataObject
{
    /** @param Collection<string, LibraryData> $data */
    public function __construct(
        protected readonly string $table,
        protected readonly Collection $data,
    ) {}

    public function __get(string $name): null|float|int|string|Carbon
    {
        $name = mb_strtoupper($name);
        if (! $this->data->has($name)) {
            throw new FieldNotFound($this->table, $name);
        }

        return $this->data[$name];
    }

    public function __isset(string $name): bool
    {
        return $this->data->has(mb_strtoupper($name));
    }

    /** @return Collection<string, LibraryData> */
    public function dump(): Collection
    {
        return $this->data;
    }
}
