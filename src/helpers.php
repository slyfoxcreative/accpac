<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Illuminate\Support\Collection;

use function SlyFoxCreative\Utilities\is_instance_of;

function isArrayOrCollection(mixed $value): bool
{
    return is_array($value) || is_instance_of($value, Collection::class);
}

function type_or_class(mixed $value): string
{
    return is_object($value) ? get_class($value) : gettype($value);
}
