<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

class TableNotFound extends \Exception
{
    public function __construct(string $table)
    {
        parent::__construct("Table '{$table}' not found");
    }
}
