<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Illuminate\Support\Collection;
use SlyFoxCreative\Accpac\Tables\QueryBuilderSelector;

class Session
{
    private readonly \PDO $session;

    private readonly Schema $schema;

    /** @param array<int, string>|Collection<int, string> $tables */
    public function __construct(
        string $hostname,
        string $username,
        string $password,
        string $database,
        array|Collection $tables = [],
    ) {
        $dsn = "sqlsrv: Server={$hostname}; Database={$database}; TrustServerCertificate=1";
        $this->session = new \PDO($dsn, $username, $password);

        // The default behavior is to fetch integer and float values as strings.
        // Turning on this attribute causes them to be converted to int and
        // float instead. Note that decimal values are always returned as
        // strings.
        $this->session->setAttribute(\PDO::SQLSRV_ATTR_FETCHES_NUMERIC_TYPE, true);

        if (is_array($tables)) {
            $tables = collect($tables);
        }

        $this->schema = new Schema($tables);
    }

    public function schema(): Schema
    {
        return $this->schema;
    }

    public function query(string $table): QueryBuilder
    {
        return QueryBuilderSelector::for($this, $table);
    }

    /** @param array<string, AccpacData>|Collection<string, AccpacData> $parameters */
    public function fetchValue(string $sql, array|Collection $parameters = []): null|float|int|string
    {
        return $this->fetchRow($sql, $parameters)?->first();
    }

    /**
     * @param  array<string, AccpacData>|Collection<string, AccpacData>  $parameters
     * @return Collection<int, AccpacData>
     * */
    public function fetchValues(string $sql, array|Collection $parameters = []): Collection
    {
        return $this->fetchRows($sql, $parameters)->map->first();
    }

    /**
     * @param  array<string, AccpacData>|Collection<string, AccpacData>  $parameters
     * @return Collection<string, AccpacData>
     */
    public function fetchRow(string $sql, array|Collection $parameters = []): ?Collection
    {
        return $this->fetchRows($sql, $parameters)->first();
    }

    /**
     * @param  array<string, AccpacData>|Collection<string, AccpacData>  $parameters
     * @return Collection<int, Collection<string, AccpacData>>
     * */
    public function fetchRows(string $sql, array|Collection $parameters = []): Collection
    {
        $statement = $this->session->prepare($sql);
        $result = $statement->execute(collect($parameters)->toArray());
        if ($result === false) {
            throw new MssqlError($statement->errorInfo()[2]);
        }

        /** @var Collection<int, Collection<string, AccpacData>> */
        return collect($statement->fetchAll(\PDO::FETCH_ASSOC))
            ->mapInto(Collection::class)
            ->map(function ($row) use ($sql) {
                return $row->map(function ($value) use ($sql) {
                    if (! is_string($value)) {
                        return $value;
                    }

                    $encoding = mb_detect_encoding($value, ['ASCII', 'UTF-8', 'ISO-8859-1']);

                    if ($encoding === false) {
                        throw new InvalidEncoding($value, $sql);
                    }

                    return mb_convert_encoding($value, 'UTF-8', $encoding);
                });
            })
        ;
    }
}
