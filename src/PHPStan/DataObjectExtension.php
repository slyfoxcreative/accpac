<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\PHPStan;

use Illuminate\Support\Str;
use PHPStan\Reflection\ClassReflection;
use PHPStan\Reflection\PropertiesClassReflectionExtension;
use PHPStan\Reflection\PropertyReflection;
use SlyFoxCreative\Accpac\DataObject;
use SlyFoxCreative\Accpac\Schema;

/**
 * Help PHPStan find whether a DataObject subclass has a property.
 */
class DataObjectExtension implements PropertiesClassReflectionExtension
{
    private Schema $schema;

    public function __construct()
    {
        $this->schema = new Schema();
    }

    public function hasProperty(ClassReflection $class, string $name): bool
    {
        // We only care about DataObject subclasses.
        if (! $class->isSubclassOf(DataObject::class)) {
            return false;
        }

        // Extract the table from the class name, e.g. OEORDHDataObject -> OEORDH.
        $table = (string) Str::of(class_basename($class->getName()))->remove('DataObject');

        // The DataObject subclass has a property if the corresponding table
        // has a field of the same name.
        return $this->schema->hasField($table, $name);
    }

    public function getProperty(ClassReflection $class, string $name): PropertyReflection
    {
        return new DataObjectPropertyReflection($this->schema, $class, $name);
    }
}
