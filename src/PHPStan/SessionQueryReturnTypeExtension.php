<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\PHPStan;

use PhpParser\Node\Expr\MethodCall;
use PhpParser\Node\Scalar\String_;
use PHPStan\Analyser\Scope;
use PHPStan\Reflection\MethodReflection;
use PHPStan\Type\DynamicMethodReturnTypeExtension;
use PHPStan\Type\NeverType;
use PHPStan\Type\ObjectType;
use PHPStan\Type\Type;
use SlyFoxCreative\Accpac\QueryBuilder;
use SlyFoxCreative\Accpac\Schema;
use SlyFoxCreative\Accpac\Session;
use SlyFoxCreative\Accpac\TableNotFound;

/**
 * Help PHPStan find the return type of the Session::query() method.
 *
 * Session::query() returns a QueryBuilder subclass based on the table being
 * queried, e.g. 'oeordh' -> OEORDHQueryBuilder
 */
class SessionQueryReturnTypeExtension implements DynamicMethodReturnTypeExtension
{
    private Schema $schema;

    public function __construct()
    {
        $this->schema = new Schema();
    }

    public function getClass(): string
    {
        return Session::class;
    }

    public function isMethodSupported(MethodReflection $method): bool
    {
        return $method->getName() === 'query';
    }

    public function getTypeFromMethodCall(
        MethodReflection $method,
        MethodCall $call,
        Scope $scope,
    ): ?Type {
        $arg = $call->getArgs()[0]->value;

        // If the argument passed to the method is a literal string, construct
        // the QueryBuilder subclass from the string value.
        if (is_a($arg, String_::class)) {
            // If the table is not found, the query method will throw a
            // TableNotFound exception, so the return type is 'never'.
            try {
                $table = $this->schema->normalizeTableName($arg->value);
            } catch (TableNotFound $e) {
                return new NeverType();
            }

            return new ObjectType("SlyFoxCreative\\Accpac\\Tables\\{$table}QueryBuilder");
        }

        // If the argument is not a literal string, i.e. if it's a variable,
        // then we can't determine the table statically and can only know that
        // the return type should be a QueryBuilder of some kind.
        return new ObjectType(QueryBuilder::class);
    }
}
