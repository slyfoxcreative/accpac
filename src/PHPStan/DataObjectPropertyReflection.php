<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\PHPStan;

use Carbon\Carbon;
use Illuminate\Support\Str;
use PHPStan\Reflection\ClassReflection;
use PHPStan\Reflection\PropertyReflection;
use PHPStan\TrinaryLogic;
use PHPStan\Type\FloatType;
use PHPStan\Type\IntegerType;
use PHPStan\Type\NeverType;
use PHPStan\Type\NullType;
use PHPStan\Type\ObjectType;
use PHPStan\Type\StringType;
use PHPStan\Type\Type;
use PHPStan\Type\UnionType;
use SlyFoxCreative\Accpac\FieldNotFound;
use SlyFoxCreative\Accpac\LibraryType;
use SlyFoxCreative\Accpac\Schema;

/**
 * Help PHPStan find the type of a DataObject-subclass property.
 */
class DataObjectPropertyReflection implements PropertyReflection
{
    private string $table;

    public function __construct(
        private readonly Schema $schema,
        private readonly ClassReflection $class,
        private readonly string $field,
    ) {
        $this->table = (string) Str::of(class_basename($this->class->getName()))->remove('DataObject');
    }

    public function getDeclaringClass(): ClassReflection
    {
        return $this->class;
    }

    public function isStatic(): bool
    {
        return false;
    }

    public function isPrivate(): bool
    {
        return false;
    }

    public function isPublic(): bool
    {
        return true;
    }

    public function getDocComment(): ?string
    {
        return null;
    }

    public function getReadableType(): Type
    {
        // If the field doesn't exist on the table, an attempt to access the
        // property will throw a FieldNotFound exception, so the return type
        // is 'never'.
        try {
            $type = $this->schema->libraryType($this->table, $this->field);
        } catch (FieldNotFound $e) {
            return new NeverType();
        }

        return match ($type) {
            LibraryType::Date => new UnionType([
                new NullType(),
                new ObjectType(Carbon::class),
            ]),

            LibraryType::Decimal => new FloatType(),

            LibraryType::Integer => new IntegerType(),

            LibraryType::String, LibraryType::Binary, LibraryType::Image => new StringType(),
        };
    }

    public function getWritableType(): Type
    {
        return new NeverType();
    }

    public function canChangeTypeAfterAssignment(): bool
    {
        return false;
    }

    public function isReadable(): bool
    {
        return true;
    }

    public function isWritable(): bool
    {
        return false;
    }

    public function isDeprecated(): TrinaryLogic
    {
        return TrinaryLogic::createNo();
    }

    public function getDeprecatedDescription(): ?string
    {
        return null;
    }

    public function isInternal(): TrinaryLogic
    {
        return TrinaryLogic::createNo();
    }
}
