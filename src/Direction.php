<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

enum Direction: string
{
    case Ascending = 'ASC';

    case Descending = 'DESC';
}
