<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Carbon\Carbon;

enum LibraryType: string
{
    case Binary = 'binary';

    case Date = 'date';

    case Decimal = 'decimal';

    case Image = 'image';

    case Integer = 'int';

    case String = 'char';

    public static function fromAccpacType(AccpacType $type): self
    {
        return match ($type) {
            AccpacType::BigInteger => self::Integer,
            AccpacType::Binary => self::Binary,
            AccpacType::Decimal => self::Decimal,
            AccpacType::Image => self::Image,
            AccpacType::Integer => self::Integer,
            AccpacType::SmallInteger => self::Integer,
            AccpacType::String => self::String,
        };
    }

    public function convert(null|float|int|string $value): null|float|int|string|Carbon
    {
        return match ($this) {
            // Dates fields in the database mostly use the decimal type instead
            // of a date type, so a date value of '0' indicates a missing value.
            self::Date => $value === '0' ? null : Carbon::parse(strval($value)),

            self::Decimal => floatval($value),

            self::Integer => intval($value),

            self::String => trim(strval($value)),

            self::Binary, self::Image => $value,
        };
    }
}
