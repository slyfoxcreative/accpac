<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

class FieldNotFound extends \Exception
{
    public function __construct(string $table, string $column)
    {
        parent::__construct("Field '{$column}' not found in table '{$table}'");
    }
}
