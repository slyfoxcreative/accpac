<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use function SlyFoxCreative\Utilities\assert_callable;

/**
 * Provides a fluent API for adding a condition from a constrained set of
 * allowed conditions to a query builder.
 */
class ConstrainedConditionBuilder
{
    /** @param array<int, string> $methods */
    public function __construct(
        private readonly QueryBuilder|ConditionBuilder $builder,
        private readonly ConditionBuilder $conditions,
        private readonly array $methods,
    ) {}

    /** @param array<int, mixed> $arguments */
    public function __call(string $name, array $arguments): QueryBuilder|ConditionBuilder
    {
        if (! in_array($name, $this->methods, strict: true)) {
            $class = self::class;

            throw new \BadMethodCallException("Call to undefined method {$class}::{$name}()");
        }

        $callable = [$this->conditions, $name];
        assert_callable($callable);
        call_user_func_array($callable, $arguments);

        return $this->builder;
    }
}
