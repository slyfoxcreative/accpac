<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

class DatabaseNotRegistered extends \Exception
{
    public function __construct(string $database)
    {
        parent::__construct("Database '{$database}' has not been registered");
    }
}
