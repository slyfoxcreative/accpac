<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

class InvalidEncoding extends \Exception
{
    public function __construct(string $value, string $sql)
    {
        parent::__construct("Invalid string value '{$value}' was returned by query: {$sql}");
    }
}
