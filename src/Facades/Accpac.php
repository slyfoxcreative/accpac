<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Facades;

use Illuminate\Support\Facades\Facade;
use SlyFoxCreative\Accpac\QueryBuilder;

/**
 * @method static QueryBuilder query(string $table)
 */
class Accpac extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'slyfoxcreative.accpac.default';
    }
}
