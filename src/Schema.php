<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

use function SlyFoxCreative\Utilities\assert_array;
use function SlyFoxCreative\Utilities\assert_instance_of;
use function SlyFoxCreative\Utilities\is_instance_of;

/**
 * Encapsulates information about the database schema.
 *
 * It's used to normalize table and field names and verify that they exist in
 * the database. It can also register custom type conversions to, for example,
 * convert a date field that is stored as a decimal in the database to a Carbon
 * date-time object.
 */
class Schema
{
    /** @var Collection<int, string> */
    public readonly Collection $tables;

    /** @var Collection<string, Collection<string, AccpacType>> */
    private readonly Collection $accpacTypes;

    /** @var Collection<string, Collection<string, LibraryType>> */
    private readonly Collection $libraryTypes;

    /** @param Collection<int, string> $tables */
    public function __construct(Collection $tables = new Collection())
    {
        $accpacTypes = $this->readAccpacSchema();
        $libraryTypes = $this->readLibrarySchema();

        $this->tables = $accpacTypes->keys();

        if ($tables->isNotEmpty()) {
            $tables = $tables->map(fn($t) => $this->normalizeTableName($t));
            $accpacTypes = $accpacTypes->filter(fn($v, $k) => $tables->contains($k));
            $libraryTypes = $libraryTypes->filter(fn($v, $k) => $tables->contains($k));
        }

        $this->accpacTypes = $accpacTypes;
        $this->libraryTypes = $libraryTypes;
    }

    /**
     * Convert the given row of values from accpac types to library types.
     *
     * @param  Collection<string, AccpacData>  $row
     * @return Collection<string, LibraryData>
     */
    public function convertValues(Collection $row, string $table): Collection
    {
        $table = $this->normalizeTableName($table);
        $row = $row->keyBy(
            fn($_, $k) => $this->normalizeFieldName($table, $k),
        );

        return $row->map(
            fn($value, $field) => $this->convertValue($value, $table, $field),
        );
    }

    /**
     * Convert the given value from its library type to its accpac type.
     */
    public function convertValueForQuery(
        string $table,
        string $field,
        float|int|string|Carbon $value,
    ): int|string {
        $table = $this->normalizeTableName($table);
        $field = $this->normalizeFieldName($table, $field);
        assert_instance_of($this->accpacTypes[$table], Collection::class);
        assert_instance_of($this->accpacTypes[$table][$field], AccpacType::class);

        switch ($this->libraryType($table, $field)) {
            case LibraryType::Date:
                if (! is_instance_of($value, Carbon::class)) {
                    throw new \TypeError(
                        "Field '{$field}' in table '{$table}' expects date, got " . type_or_class($value),
                    );
                }

                break;

            case LibraryType::Decimal:
            case LibraryType::Integer:
                if (! is_numeric($value)) {
                    throw new \TypeError(
                        "Field '{$field}' in table '{$table}' expects number, got " . type_or_class($value),
                    );
                }

                break;

            case LibraryType::String:
            case LibraryType::Binary:
            case LibraryType::Image:
                if (! is_string($value)) {
                    throw new \TypeError(
                        "Field '{$field}' in table '{$table}' expects string, got " . type_or_class($value),
                    );
                }

                break;

            default:
                throw new \Exception('unreachable');
        }

        return $this->accpacTypes[$table][$field]->convert($value);
    }

    /**
     * Normalize the given table name to the standard format for table names.
     */
    public function normalizeTableName(string $table): string
    {
        $table = mb_strtoupper($table);

        if (! $this->tables->contains($table)) {
            throw new TableNotFound($table);
        }

        return $table;
    }

    /**
     * Normalize the given field name to the standard format for field names.
     */
    public function normalizeFieldName(string $table, string $field): string
    {
        $table = mb_strtoupper($table);
        $field = mb_strtoupper($field);

        if (!isset($this->accpacTypes[$table][$field])) {
            throw new FieldNotFound($table, $field);
        }

        return $field;
    }

    /**
     * Return wether the given table has the given field.
     */
    public function hasField(string $table, string $field): bool
    {
        $table = $this->normalizeTableName($table);
        $field = mb_strtoupper($field);

        return isset($this->accpacTypes[$table][$field]);
    }

    /**
     * Return the library type for the given field.
     */
    public function libraryType(string $table, string $field): LibraryType
    {
        $table = $this->normalizeTableName($table);
        $field = $this->normalizeFieldName($table, $field);
        assert_instance_of($this->accpacTypes[$table], Collection::class);
        assert_instance_of($this->accpacTypes[$table][$field], AccpacType::class);

        if (isset($this->libraryTypes[$table][$field])) {
            return $this->libraryTypes[$table][$field];
        }

        // Default to the corresponding library type for the field's
        // accpac type.
        return LibraryType::fromAccpacType($this->accpacTypes[$table][$field]);
    }

    /**
     * Convert the given value from its accpac type to its library type.
     */
    private function convertValue(
        null|float|int|string $value,
        string $table,
        string $field,
    ): null|float|int|string|Carbon {
        return $this->libraryType($table, $field)->convert($value);
    }

    /**
     * Read the schema for accpac database types.
     *
     * @return Collection<string, Collection<string, AccpacType>>
     */
    private function readAccpacSchema(): Collection
    {
        $schema = $this->readSchema(__DIR__ . '/../schema/accpac_types.json');

        $this->validateSchema($schema, [
            'bigint',
            'binary',
            'char', 'decimal',
            'image',
            'int',
            'smallint',
        ]);

        return collect($schema)->map(
            fn($c) => collect($c)->map(
                fn($s) => AccpacType::from($s),
            ),
        );
    }

    /**
     * Read the schema for library types.
     *
     * @return Collection<string, Collection<string, LibraryType>>
     */
    private function readLibrarySchema(): Collection
    {
        $schema = $this->readSchema(__DIR__ . '/../schema/library_types.json');

        $this->validateSchema($schema, [
            'bigint',
            'binary',
            'char',
            'date',
            'decimal',
            'image',
            'int',
            'smallint',
        ]);

        return collect($schema)->map(
            fn($c) => collect($c)->map(
                fn($s) => LibraryType::from($s),
            ),
        );
    }

    /**
     * Read a database schema from a JSON file.
     */
    private function readSchema(string $path): mixed
    {
        $schema = file_get_contents($path);

        if ($schema === false) {
            throw new \Exception("Failed to read {$path}");
        }

        $schema = json_decode($schema, true);

        if (is_null($schema)) {
            throw new \Exception("Failed to parse {$path}");
        }

        return $schema;
    }

    /**
     * Validate the given schema data.
     *
     * @param  array<int, string>  $types
     *
     * @phpstan-assert array<string, array<string, string>> $schema
     */
    private function validateSchema(mixed $schema, array $types): void
    {
        $types = collect($types);

        assert_array($schema);
        foreach ($schema as $table => $fields) {
            if (! $this->validateName($table)) {
                throw new \Exception("Invalid schema table name '{$table}'");
            }

            assert_array($fields);
            foreach ($fields as $field => $value) {
                if (! $this->validateName($field) || ! $types->contains($value)) {
                    throw new \Exception("Invalid schema field name '{$field}' in table '{$table}'");
                }
            }
        }
    }

    /**
     * Validate that a table or field name value is an uppercase string.
     */
    private function validateName(mixed $name): bool
    {
        return is_string($name) && Str::of($name)->isMatch('/^[A-Z0-9]+$/');
    }
}
