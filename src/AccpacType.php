<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Carbon\Carbon;

use function SlyFoxCreative\Utilities\is_instance_of;

enum AccpacType: string
{
    case BigInteger = 'bigint';

    case Binary = 'binary';

    case Decimal = 'decimal';

    case Image = 'image';

    case Integer = 'int';

    case SmallInteger = 'smallint';

    case String = 'char';

    public function convert(float|int|string|Carbon $value): int|string
    {
        switch ($this) {
            case self::Decimal:
                if (! is_instance_of($value, Carbon::class) && ! is_numeric($value)) {
                    throw new \TypeError('Expected date or number, got ' . gettype($value));
                }

                return is_instance_of($value, Carbon::class)
                    ? $value->format('Ymd')
                    : strval($value);

            case self::BigInteger:
            case self::Integer:
            case self::SmallInteger:
                if (! is_int($value)) {
                    throw new \TypeError('Expected integer, got ' . gettype($value));
                }

                return $value;

            case self::Binary:
            case self::Image:
            case self::String:
                if (! is_string($value)) {
                    throw new \TypeError('Expected string, got ' . gettype($value));
                }

                return $value;

            default:
                throw new \Exception('unreachable');
        }
    }
}
