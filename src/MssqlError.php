<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

class MssqlError extends \Exception
{
    public function __construct(string $message)
    {
        parent::__construct("MSSQL error: {$message}");
    }
}
