<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

use function SlyFoxCreative\Utilities\assert_array;

class ServiceProvider extends BaseServiceProvider
{
    private const CONFIG_FILE = __DIR__ . '/config/accpac.php';

    public function boot(): void
    {
        $this->publishes([
            self::CONFIG_FILE => config_path(basename(self::CONFIG_FILE)),
        ]);
    }

    public function register(): void
    {
        $this->mergeConfigFrom(
            self::CONFIG_FILE,
            basename(self::CONFIG_FILE, '.php'),
        );

        foreach ((array) config('accpac.databases') as $name => $c) {
            $this->app->singleton(
                "slyfoxcreative.accpac.{$name}",
                function ($app) use ($c) {
                    assert_array($c);

                    return new Session(
                        $c['hostname'],
                        $c['username'],
                        $c['password'],
                        $c['database'],
                        $c['tables'] ?? [],
                    );
                },
            );
        }
    }
}
