<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * Builds a WHERE clause with a list of conditions.
 */
class ConditionBuilder
{
    /** @var Collection<int, string> */
    private Collection $conditions;

    private bool $negate;

    private bool $or;

    public function __construct(
        private readonly QueryBuilder|ConditionBuilder $builder,
        private readonly Schema $schema,
        private readonly string $table,
        private ParameterList $params,
    ) {
        $this->conditions = new Collection();
        $this->negate = false;
        $this->or = false;
    }

    /**
     * Return the SQL string for the WHERE clause.
     */
    public function __toString(): string
    {
        if ($this->conditions->isEmpty()) {
            return '';
        }

        return trim($this->conditions->join(' '));
    }

    /**
     * Add an '=' condition.
     */
    public function eq(string $field, float|int|string|Carbon $value): self
    {
        return $this->condition($field, '=', $value);
    }

    /**
     * Add a '<>' condition.
     */
    public function ne(string $field, float|int|string|Carbon $value): self
    {
        return $this->condition($field, '<>', $value);
    }

    /**
     * Add an 'IN' condition.
     *
     * @param  array<int, QueryData>|Collection<int, QueryData>  $values
     */
    public function in(string $field, array|Collection $values): self
    {
        if (is_array($values)) {
            $values = collect($values);
        }

        $ps = $values
            ->map(fn($v) => $this->params->add($this->table, $field, $v))
            ->join(', ')
        ;
        $sql = $this->sql($field, 'IN', "({$ps})");
        $this->add($sql);

        return $this;
    }

    /**
     * Add a 'LIKE' condition.
     */
    public function like(string $field, float|int|string|Carbon $value): self
    {
        return $this->condition($field, 'LIKE', $value);
    }

    /**
     * Add a '>' condition.
     */
    public function gt(string $field, float|int|string|Carbon $value): self
    {
        return $this->condition($field, '>', $value);
    }

    /**
     * Add a '>=' condition.
     */
    public function ge(string $field, float|int|string|Carbon $value): self
    {
        return $this->condition($field, '>=', $value);
    }

    /**
     * Add a '<' condition.
     */
    public function lt(string $field, float|int|string|Carbon $value): self
    {
        return $this->condition($field, '<', $value);
    }

    /**
     * Add a '<=' condition.
     */
    public function le(string $field, float|int|string|Carbon $value): self
    {
        return $this->condition($field, '<=', $value);
    }

    /**
     * Add an IS NULL condition.
     */
    public function null(string $field): self
    {
        $sql = $this->sql($field, 'NULL', null);
        $this->add($sql);

        return $this;
    }

    /**
     * Negate the next condition.
     */
    public function not(): ConstrainedConditionBuilder
    {
        $this->negate = true;

        return new ConstrainedConditionBuilder(
            $this->builder,
            $this,
            ['eq', 'ne', 'in', 'like', 'gt', 'ge', 'lt', 'le', 'null', 'paren'],
        );
    }

    /**
     * Add the next condition with an OR conjunction.
     */
    public function or(): ConstrainedConditionBuilder
    {
        $this->or = true;

        return new ConstrainedConditionBuilder(
            $this->builder,
            $this,
            ['eq', 'ne', 'in', 'like', 'gt', 'ge', 'lt', 'le', 'null', 'not', 'paren'],
        );
    }

    /**
     * Add a parenthesized grouping of conditions.
     */
    public function paren(callable $conditions): self
    {
        $builder = new ConditionBuilder($this, $this->schema, $this->table, $this->params);
        $conditions($builder);

        $this->add("({$builder})");

        return $this;
    }

    /**
     * Add a condition.
     *
     * @param  string  $field  The field to be compared
     * @param  string  $op  The comparison operator
     * @param  Carbon|float|int|string  $value  The value to compare with the field
     */
    private function condition(
        string $field,
        string $op,
        float|int|string|Carbon $value,
    ): self {
        $p = $this->params->add($this->table, $field, $value);
        $sql = $this->sql($field, $op, $p);
        $this->add($sql);

        return $this;
    }

    /**
     * Return the SQL string for a condition.
     *
     * @param  string  $field  The field to be compared
     * @param  string  $op  The comparison operator
     * @param  string  $param  The parameter to compare with
     */
    private function sql(string $field, string $op, ?string $param = null): string
    {
        $field = $this->schema->normalizeFieldName($this->table, $field);

        switch ($op) {
            case 'NULL':
                if ($this->negate) {
                    $this->negate = false;

                    return "[{$field}] IS NOT NULL";
                }

                return "[{$field}] IS NULL";

            case 'IN':
                // If the list is empty...
                if ($param === '()') {
                    if ($this->negate) {
                        // Add a condition that is always true.
                        $this->negate = false;

                        return $this->sql($field, '=', "[{$field}]");
                    }

                    // Add a condition that is always false.
                    return $this->sql($field, '<>', "[{$field}]");
                }

                // no break
            default:
                return "[{$field}] {$op} {$param}";
        }
    }

    /**
     * Add the SQL string for a condition to the lsit of conditions, with the
     * appropriate conjunction and negation.
     *
     * @param  string  $condition  The SQL string for the condition
     */
    private function add(string $condition): void
    {
        $not = $this->negate ? 'NOT' : '';
        $con = $this->conditions->isEmpty()
            ? ''
            : ($this->or ? 'OR' : 'AND');
        $this->conditions->push("{$con} {$not} {$condition}");

        $this->negate = false;
        $this->or = false;
    }
}
