<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tables;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SlyFoxCreative\Accpac\QueryBuilder;
use SlyFoxCreative\Accpac\Session;

class GLRVRATQueryBuilder extends QueryBuilder
{
    public function __construct(Session $session, string $table)
    {
        $classTable = (string) Str::of(class_basename(self::class))->remove('QueryBuilder');

        if ($classTable !== $table) {
            throw new \ValueError("Tried to make a {$table} query builder from {$table} data");
        }

        parent::__construct($session, $table);
    }

    /** @return Collection<int, GLRVRATDataObject> */
    public function get(): Collection
    {
        return parent::getBase()->mapInto(GLRVRATDataObject::class);
    }

    public function first(): ?GLRVRATDataObject
    {
        return $this->get()->first();
    }
}
