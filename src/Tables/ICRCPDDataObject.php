<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tables;

use Illuminate\Support\Str;
use SlyFoxCreative\Accpac\DataObject;

class ICRCPDDataObject extends DataObject
{
    public function __construct(DataObject $object)
    {
        $table = (string) Str::of(class_basename(self::class))->remove('DataObject');

        if ($table !== $object->table) {
            throw new \ValueError("Tried to make a {$table} from {$object->table} data");
        }

        parent::__construct($object->table, $object->data);
    }
}
