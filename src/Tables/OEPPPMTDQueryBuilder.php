<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tables;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use SlyFoxCreative\Accpac\QueryBuilder;
use SlyFoxCreative\Accpac\Session;

class OEPPPMTDQueryBuilder extends QueryBuilder
{
    public function __construct(Session $session, string $table)
    {
        $classTable = (string) Str::of(class_basename(self::class))->remove('QueryBuilder');

        if ($classTable !== $table) {
            throw new \ValueError("Tried to make a {$table} query builder from {$table} data");
        }

        parent::__construct($session, $table);
    }

    /** @return Collection<int, OEPPPMTDDataObject> */
    public function get(): Collection
    {
        return parent::getBase()->mapInto(OEPPPMTDDataObject::class);
    }

    public function first(): ?OEPPPMTDDataObject
    {
        return $this->get()->first();
    }
}
