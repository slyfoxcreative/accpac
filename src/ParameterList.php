<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac;

use Carbon\Carbon;
use Illuminate\Support\Collection;

/**
 * A list of parameters used in a query.
 */
class ParameterList
{
    /** @var Collection<int, AccpacData> */
    private Collection $params;

    public function __construct(private Schema $schema)
    {
        $this->params = new Collection();
    }

    /**
     * Return the list of parameter names.
     *
     * @return Collection<string, AccpacData>
     */
    public function get(): Collection
    {
        return $this->params->keyBy(fn($_, $i) => ":p{$i}");
    }

    /**
     * Add a parameter to the list.
     *
     * Returns the name of the added parameter.
     */
    public function add(
        string $table,
        string $field,
        float|int|string|Carbon $value,
    ): string {
        $value = $this->schema->convertValueForQuery($table, $field, $value);
        $this->params->push($value);
        $i = $this->params->count() - 1;

        return ":p{$i}";
    }
}
