<?php

declare(strict_types=1);

return [
    'databases' => [
        'default' => [
            'hostname' => env('ACCPAC_HOSTNAME'),
            'username' => env('ACCPAC_USERNAME'),
            'password' => env('ACCPAC_PASSWORD'),
            'database' => env('ACCPAC_DATABASE'),
            'tables' => [],
        ],
    ],
];
