# Test Products

## MIS\*TEST1

Has all possible data.

| Name                         | Value                          | Readable Value |
| ---------------------------- | ------------------------------ | -------------- |
| Item Number                  | MISTEST1                       | MIS\*TEST1     |
| Description                  | DO NOT USE (For testing only)À |                |
| Price List                   | STAND                          |                |
| Price                        | 50                             |                |
| Discount Level 1             | 40.5                           |                |
| Discount Level 2             | 30.0                           |                |
| Discount Level 3             | 20.0                           |                |
| Discount Level 4             | 10.0                           |                |
| Discount Level 5             | 0.0                            |                |
| Special Price                | 25                             |                |
| Special Start                | 20190930                       | 2019-09-30     |
| Special End                  | 29991231                       | 2999-12-31     |
| Quantity On Hand             | 25                             |                |
| Quantity Committed           | 17                             |                |
| Quantity On Sales Order      | 20                             |                |
| Quantity Shipped Not Costed  | 0                              |                |
| Quantity Received Not Costed | 0                              |                |
| Quantity Adjusted Not Costed | 0                              |                |
| GTIN                         | 123456789012                   |                |
| Weight                       | 5                              |                |
| Height                       | 6                              |                |
| Length                       | 12                             |                |
| Width                        | 10                             |                |
| Drop Ship                    | 0                              | no             |
| Freight Class                | 77.5                           |                |
| NMFC                         | 111220-06                      |                |

## MIS\*TEST2

Does not have optional data.

| Name                         | Value                         | Readable Value |
| ---------------------------- | ----------------------------- | -------------- |
| Item Number                  | MISTEST2                      | MIS\*TEST2     |
| Description                  | DO NOT USE (For testing only) |                |
| Price List                   | STAND                         |                |
| Price                        | 50                            |                |
| Discount Level 1             | 40.0                          |                |
| Discount Level 2             | 30.0                          |                |
| Discount Level 3             | 20.0                          |                |
| Discount Level 4             | 10.0                          |                |
| Discount Level 5             | 5.0                           |                |
| Special Price                | 0                             | —              |
| Special Start                | 0                             | —              |
| Special End                  | 0                             | —              |
| Quantity On Hand             | 10                            |                |
| Quantity Committed           | 2                             |                |
| Quantity On Sales Order      | 2                             |                |
| Quantity Shipped Not Costed  | 0                             |                |
| Quantity Received Not Costed | 0                             |                |
| Quantity Adjusted Not Costed | 0                             |                |
| GTIN                         | —                             |                |
| Weight                       | 5                             |                |
| Height                       | 6                             |                |
| Length                       | 12                            |                |
| Width                        | 10                            |                |
| Drop Ship                    | 0                             | no             |
| Freight Class                | 77.5                          |                |
| NMFC                         | 111220-06                     |                |

## MIS\*TEST3

Missing required data.

| Name                         | Value                         | Readable Value |
| ---------------------------- | ----------------------------- | -------------- |
| Item Number                  | MISTEST3                      | MIS\*TEST3     |
| Description                  | DO NOT USE (For testing only) |                |
| Price List                   | STAND                         |                |
| Price                        | —                             |                |
| Discount Level 1             | —                             |                |
| Discount Level 2             | —                             |                |
| Discount Level 3             | —                             |                |
| Discount Level 4             | —                             |                |
| Discount Level 5             | —                             |                |
| Special Price                | —                             |                |
| Special Start                | —                             |                |
| Special End                  | —                             |                |
| Quantity On Hand             | —                             |                |
| Quantity Committed           | —                             |                |
| Quantity On Sales Order      | —                             |                |
| Quantity Shipped Not Costed  | —                             |                |
| Quantity Received Not Costed | —                             |                |
| Quantity Adjusted Not Costed | —                             |                |
| GTIN                         | —                             |                |
| Weight                       | 0                             |                |
| Height                       | —                             |                |
| Length                       | —                             |                |
| Width                        | —                             |                |
| Drop Ship                    | —                             |                |
| Freight Class                | —                             |                |
| NMFC                         | —                             |                |

## MIS\*TEST4

Does not exist in Sage at all.

## MIS\*TEST5-KIT

Has kits.

| Name                         | Value                         | Readable Value |
| ---------------------------- | ----------------------------- | -------------- |
| Item Number                  | MISTEST5-KIT                  | MIS\*TEST5-KIT |
| Description                  | DO NOT USE (For testing only) |                |
| Price List                   | STAND                         |                |
| Price                        | 50                            |                |
| Discount Level 1             | 40.0                          |                |
| Discount Level 2             | 30.0                          |                |
| Discount Level 3             | 20.0                          |                |
| Discount Level 4             | 10.0                          |                |
| Discount Level 5             | 5.0                           |                |
| Special Price                | 0                             | —              |
| Special Start                | 0                             | —              |
| Special End                  | 0                             | —              |
| Quantity On Hand             | —                             |                |
| Quantity Committed           | —                             |                |
| Quantity On Sales Order      | —                             |                |
| Quantity Shipped Not Costed  | —                             |                |
| Quantity Received Not Costed | —                             |                |
| Quantity Adjusted Not Costed | —                             |                |
| GTIN                         | —                             |                |
| Weight                       | 5                             |                |
| Height                       | 6                             |                |
| Length                       | 12                            |                |
| Width                        | 10                            |                |
| Drop Ship                    | 0                             | no             |
| Freight Class                | 77.5                          |                |
| NMFC                         | 111220-06                     |                |

## Kits

| Item Number  | Kit   |
| ------------ | ----- |
| MISTEST5-KIT | BLACK |
| MISTEST5-KIT | WHITE |
