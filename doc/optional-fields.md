# Optional Fields

| Name   | Label         | Type    |
| ------ | ------------- | ------- |
| DROP   | Drop Ship     | Yes/No  |
| LENGTH | Length        | Integer |
| WIDTH  | Width         | Integer |
| HEIGHT | Height        | Integer |
| CLASS  | Freight Class | Text    |
| NMFC   | NMFC          | Text    |
