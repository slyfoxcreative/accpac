# Test Order Items (OEORDD)

## TEST1

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 16       |

## TEST2

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 1        |

## TEST3

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 0        |
| MIS\*TEST2  |                | 2        |

## TEST4

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 16       |

## TEST5

| Item Number    | Kitting Number | Quantity |
| -------------- | -------------- | -------- |
| MIS\*TEST1     |                | 1        |
| MIS\*TEST5-KIT | BLACK          | 1        |
| MIS\*TEST5-KIT | WHITE          | 1        |

## TEST6

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 8        |

## TEST6-B

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 8        |

## TEST7

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 1        |

## TEST12

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 0        |

## TEST13

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 1        |

## TEST14

| Item Number | Kitting Number | Quantity |
| ----------- | -------------- | -------- |
| MIS\*TEST1  |                | 1        |

## TEST16

| Item Number      | Kitting Number | Quantity |
| ---------------- | -------------- | -------- |
| MIS\*TEST1       |                | 1        |
| WEB\*STICKER     |                | 1        |
| MIS\*MISC-CHARGE |                | 1        |
| MIS\*NON_STOCK   |                | 1        |
