# Test Orders (OEORDH, OEORDH1)

## TEST1

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST1              |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST2

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST2              |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST3

Has one line item with a zeroed quantity.

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST3              |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST4

All items shipped in one shipment.

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST4              |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST5

All items shipped in two shipments.

The second shipment is missing its tracking number.

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST5              |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST6

First order in a multi-order set.

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST6              |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST6-B

Second order in a multi-order set.

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST6-B            |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST7

The shipment is missing its tracking number but the tracking number
is on the order.

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST7              |
| Tracking Number    | 12345              |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST12

Has an invalid country code in the destination address.

| Name               | Value       |
| ------------------ | ----------- |
| Number             | TEST12      |
| Tracking Number    |             |
| Shipping Address 1 | 123 Fake St |
| Shipping City      | Fake City   |
| Shipping State     | FAKESTATE   |
| Shipping ZIP Code  | 12345       |
| Shipping Country   | FAKECOUNTRY |

# TEST13

The shipment is missing its tracking number but the tracking number
is on the invoice.

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST13             |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST14

The shipment is CPU (store pickup).

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST14             |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |

# TEST16

Has WEB\*STICKER, MIS\*MISC-CHARGE and MIS\*NON_STOCK items.

| Name               | Value              |
| ------------------ | ------------------ |
| Number             | TEST16             |
| Tracking Number    |                    |
| Shipping Address 1 | 1111 S Figueroa St |
| Shipping City      | Los Angeles        |
| Shipping State     | CA                 |
| Shipping ZIP Code  | 90015              |
| Shipping Country   | USA                |
