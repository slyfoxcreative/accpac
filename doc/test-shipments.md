# Test Shipments (OESHIH, OESHID)

| Shipment Number | Carrier | Tracking Number |
| --------------- | ------- | --------------- |
| SHTEST4-1       | USF     | 12345           |
| SHTEST5-1       | UPSGRD  | 12345           |
| SHTEST5-2       | UPSGRD  |                 |
| SHTEST6-1       | UPSGRD  | 12345           |
| SHTEST6-B-1     | UPSGRD  | 12345           |
| SHTEST7-1       | DROP    |                 |
| SHTEST13-1      | DROP    |                 |
| SHTEST14-1      | CPU     |                 |
| SHTEST16-1      | UPSGRD  | 12345           |
| SHTEST16-2      | UPSGRD  |                 |

## Items

| Shipment Number | Item Number      | Kit   | Quantity |
| --------------- | ---------------- | ----- | -------- |
| SHTEST4-1       | MIS\*TEST1       |       | 16       |
| SHTEST5-1       | MIS\*TEST1       |       | 1        |
| SHTEST5-1       | MIS\*TEST5-KIT   | BLACK | 1        |
| SHTEST5-1       | MIS\*TEST5-KIT   | WHITE | 0        |
| SHTEST5-2       | MIS\*TEST5-KIT   | WHITE | 1        |
| SHTEST6-1       | MIS\*TEST1       |       | 8        |
| SHTEST6-B-1     | MIS\*TEST1       |       | 8        |
| SHTEST7-1       | MIS\*TEST1       |       | 1        |
| SHTEST13-1      | MIS\*TEST1       |       | 1        |
| SHTEST14-1      | MIS\*TEST1       |       | 1        |
| SHTEST16-1      | MIS\*MISC-CHARGE |       | 1        |
| SHTEST16-1      | MIS\*NON_STOCK   |       | 1        |
| SHTEST16-1      | MIS\*TEST1       |       | 1        |
| SHTEST16-1      | WEB\*STICKER     |       | 1        |
| SHTEST16-2      | MIS\*MISC-CHARGE |       | 0        |
| SHTEST16-2      | MIS\*NON_STOCK   |       | 0        |
