<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Accpac\Session;

use function SlyFoxCreative\Utilities\assert_array;
use function SlyFoxCreative\Utilities\assert_instance_of;
use function SlyFoxCreative\Utilities\assert_int;
use function SlyFoxCreative\Utilities\assert_not_null;
use function SlyFoxCreative\Utilities\assert_string;

class SandboxOrderItemsTest extends TestCase
{
    private static Session $session;

    /** @var Collection<string, Collection<int, TestOrderItem>> */
    private static Collection $orders;

    public static function setUpBeforeClass(): void
    {
        self::$session = new Session(
            $_ENV['ACCPAC_HOSTNAME'],
            $_ENV['ACCPAC_USERNAME'],
            $_ENV['ACCPAC_PASSWORD'],
            $_ENV['ACCPAC_DATABASE'],
            ['oeordh', 'oeordd'],
        );

        $lines = file('./doc/test-order-items.md');

        if ($lines === false) {
            throw new \Exception('Failed to read ./doc/test-order-items.md');
        }

        $lines = collect($lines)
            ->mapInto(Stringable::class)
            ->reject->startsWith("\n")
            ->reject->startsWith('# ')
            ->reject->startsWith('| -')
            ->reject->startsWith('| Item')
        ;

        $orders = new Collection();
        $orderNumber = null;

        foreach ($lines as $line) {
            if ($line->startsWith('##')) {
                $orderNumber = (string) $line->trim("# \n");
                $orders->put($orderNumber, new Collection());
            } else {
                $cells = $line
                    ->trim("| \n")
                    ->explode('|')
                    ->mapInto(Stringable::class)
                    ->map->trim()
                ;

                assert_not_null($orders[$orderNumber]);
                assert_not_null($cells[0]);
                $orders[$orderNumber]->push([
                    'item_number' => (string) $cells[0]->remove('\\'),
                    'kitting_number' => (string) $cells[1],
                    'quantity' => (string) $cells[2],
                ]);
            }
        }

        self::verifyOrders($orders);
        self::$orders = $orders;
    }

    /** @phpstan-assert Collection<string, Collection<int, TestOrderItem>> $collection */
    private static function verifyOrders(mixed $collection): void
    {
        assert_instance_of($collection, Collection::class);
        foreach ($collection as $number => $orders) {
            assert_string($number);
            assert_instance_of($orders, Collection::class);
            foreach ($orders as $index => $order) {
                assert_int($index);
                assert_array($order);
                assert_string($order['item_number']);
                assert_string($order['quantity']);
            }
        }
    }

    public function testItems(): void
    {
        self::$orders->each(function ($expectedItems, $orderNumber) {
            $order = self::$session
                ->query('oeordh')
                ->eq('ordnumber', $orderNumber)
                ->first()
            ;

            self::assertNotNull($order, $orderNumber);

            $items = self::$session
                ->query('oeordd')
                ->eq('orduniq', $order->orduniq)
                ->get()
            ;

            $expectedItems->zip($items)->each(function ($value) use ($orderNumber) {
                [$expected, $item] = $value;

                $message = "{$orderNumber} / {$expected['item_number']}";

                self::assertNotNull($expected, $message);
                self::assertNotNull($item, $message);

                self::assertSame($expected['item_number'], $item->item, $message);
                self::assertSame($expected['kitting_number'], $item->ddtlno, $message);
                self::assertSame(floatval($expected['quantity']), $item->origqty, $message);
            });
        });
    }
}
