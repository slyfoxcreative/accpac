<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use SlyFoxCreative\Accpac\Facades\Accpac;

class LaravelTest extends TestCase
{
    public function testLaravelIntegration(): void
    {
        $object = Accpac::query('icitem')
            ->select('unitwgt')
            ->eq('itemno', 'MISTEST1')
            ->first()
        ;

        self::assertNotNull($object);
        self::assertSame(5.0, $object->unitwgt);
    }
}
