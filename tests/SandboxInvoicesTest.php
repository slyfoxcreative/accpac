<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Accpac\Session;

use function SlyFoxCreative\Utilities\assert_array;
use function SlyFoxCreative\Utilities\assert_instance_of;
use function SlyFoxCreative\Utilities\assert_int;
use function SlyFoxCreative\Utilities\assert_string;

class SandboxInvoicesTest extends TestCase
{
    private static Session $session;

    /** @var Collection<int, TestInvoice> */
    private static Collection $invoices;

    public static function setUpBeforeClass(): void
    {
        self::$session = new Session(
            $_ENV['ACCPAC_HOSTNAME'],
            $_ENV['ACCPAC_USERNAME'],
            $_ENV['ACCPAC_PASSWORD'],
            $_ENV['ACCPAC_DATABASE'],
            ['oeinvh'],
        );

        $lines = file('./doc/test-invoices.md');

        if ($lines === false) {
            throw new \Exception('Failed to read ./doc/test-invoices.md');
        }

        $lines = collect($lines)->mapInto(Stringable::class);

        $invoices = $lines
            ->filter->startsWith('|')
            ->reject->startsWith('| Name')
            ->reject->startsWith('| -')
            ->chunk(2)
            ->map(function ($chunk) {
                return $chunk
                    ->mapWithKeys(function ($line) {
                        $parts = $line
                            ->trim("|\n")
                            ->explode('|')
                            ->mapInto(Stringable::class)
                            ->map->trim()
                        ;

                        return [(string) $parts[0] => (string) $parts[1]];
                    })
                    ->toArray()
                ;
            })
        ;
        self::verifyInvoices($invoices);
        self::$invoices = $invoices;
    }

    /** @phpstan-assert Collection<int, TestInvoice> $collection */
    private static function verifyInvoices(mixed $collection): void
    {
        assert_instance_of($collection, Collection::class);
        foreach ($collection as $key => $value) {
            assert_int($key);
            assert_array($value);
            assert_string($value['Number']);
            assert_string($value['Tracking Number']);
        }
    }

    public function testInvoices(): void
    {
        self::$invoices->each(function ($invoice) {
            $data = self::$session
                ->query('oeinvh')
                ->eq('invnumber', $invoice['Number'])
                ->first()
            ;

            self::assertNotNull($data, $invoice['Number']);
            self::assertSame($invoice['Tracking Number'], $data->shiptrack, $invoice['Number']);
        });
    }
}
