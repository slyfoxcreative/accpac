<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Accpac\AccpacType;

class AccpacTypeTest extends TestCase
{
    public function testConvertDecimals(): void
    {
        $values = [
            [1.0, '1'],
            [1, '1'],
            [Carbon::parse('2022-01-01'), '20220101'],
        ];

        foreach ($values as [$value, $expected]) {
            $converted = AccpacType::Decimal->convert($value);
            self::assertSame($expected, $converted);
        }
    }

    public function testConvertInvalidDecimal(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected date or number, got string');

        AccpacType::Decimal->convert('invalid');
    }

    public function testConvertBigInteger(): void
    {
        self::assertSame(1, AccpacType::BigInteger->convert(1));
    }

    public function testConvertBigIntegerWithFloat(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected integer, got double');

        AccpacType::BigInteger->convert(1.0);
    }

    public function testConvertBigIntegerWithString(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected integer, got string');

        AccpacType::BigInteger->convert('1');
    }

    public function testConvertInteger(): void
    {
        self::assertSame(1, AccpacType::Integer->convert(1));
    }

    public function testConvertIntegerWithFloat(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected integer, got double');

        AccpacType::Integer->convert(1.0);
    }

    public function testConvertIntegerWithString(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected integer, got string');

        AccpacType::Integer->convert('1');
    }

    public function testConvertSmallInteger(): void
    {
        self::assertSame(1, AccpacType::SmallInteger->convert(1));
    }

    public function testConvertSmallIntegerWithFloat(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected integer, got double');

        AccpacType::SmallInteger->convert(1.0);
    }

    public function testConvertSmallIntegerWithString(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected integer, got string');

        AccpacType::SmallInteger->convert('1');
    }

    public function testConvertBinary(): void
    {
        self::assertSame('test', AccpacType::Binary->convert('test'));
    }

    public function testConvertInvalidBinary(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected string, got integer');

        AccpacType::Binary->convert(1);
    }

    public function testConvertImage(): void
    {
        self::assertSame('test', AccpacType::Image->convert('test'));
    }

    public function testConvertInvalidImage(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected string, got integer');

        AccpacType::Image->convert(1);
    }

    public function testConvertString(): void
    {
        self::assertSame('test', AccpacType::String->convert('test'));
    }

    public function testConvertInvalidString(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage('Expected string, got integer');

        AccpacType::String->convert(1);
    }
}
