<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use Carbon\Carbon;
use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Accpac\FieldNotFound;
use SlyFoxCreative\Accpac\Session;
use SlyFoxCreative\Accpac\TableNotFound;

class SchemaTest extends TestCase
{
    private static Session $session;

    public static function setUpBeforeClass(): void
    {
        self::$session = new Session(
            $_ENV['ACCPAC_HOSTNAME'],
            $_ENV['ACCPAC_USERNAME'],
            $_ENV['ACCPAC_PASSWORD'],
            $_ENV['ACCPAC_DATABASE'],
            ['iciloc', 'icitem', 'icpricp'],
        );
    }

    public function testConvertValueForQuery(): void
    {
        $values = [
            ['icitem', 'unitwgt', 1.0, '1'],
            ['iciloc', 'qtycommit', 1, '1'],
            ['icitem', 'datelastmn', Carbon::parse('2022-01-01'), '20220101'],
        ];

        foreach ($values as [$table, $field, $value, $expected]) {
            $converted = self::$session->schema()->convertValueForQuery($table, $field, $value);
            self::assertSame($expected, $converted);
        }
    }

    public function testConverValueForQueryWithInvalidDateValue(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage("Field 'DATELASTMN' in table 'ICITEM' expects date, got string");

        self::$session->schema()->convertValueForQuery('icitem', 'datelastmn', 'invalid');
    }

    public function testConverValueForQueryWithInvalidDecimalValue(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage("Field 'UNITWGT' in table 'ICITEM' expects number, got string");

        self::$session->schema()->convertValueForQuery('icitem', 'unitwgt', 'invalid');
    }

    public function testConverValueForQueryWithInvalidIntegerValue(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage("Field 'ALTSET' in table 'ICITEM' expects number, got string");

        self::$session->schema()->convertValueForQuery('icitem', 'altset', 'invalid');
    }

    public function testConverValueForQueryWithInvalidStringValue(): void
    {
        self::expectException(\TypeError::class);
        self::expectExceptionMessage("Field 'ITEMNO' in table 'ICITEM' expects string, got integer");

        self::$session->schema()->convertValueForQuery('icitem', 'itemno', 1);
    }

    public function testNormalizeTableName(): void
    {
        $table = self::$session->schema()->normalizeTableName('icitem');

        self::assertSame('ICITEM', $table);
    }

    public function testNormalizeNonexistentTableName(): void
    {
        self::expectException(TableNotFound::class);
        self::expectExceptionMessage("Table 'FAKE' not found");

        self::$session->schema()->normalizeTableName('fake');
    }

    public function testNormalizeFieldName(): void
    {
        $field = self::$session->schema()->normalizeFieldName('icitem', 'itemno');

        self::assertSame('ITEMNO', $field);
    }

    public function testNormalizeNonexistentFieldName(): void
    {
        self::expectException(FieldNotFound::class);
        self::expectExceptionMessage("Field 'FAKE' not found in table 'ICITEM'");

        self::$session->schema()->normalizeFieldName('icitem', 'fake');
    }

    public function testConversion(): void
    {
        $object = self::$session->query('icitem')->eq('itemno', 'MISTEST1')->first();

        self::assertNotNull($object);

        $rawValue = $object->dump()['DATELASTMN'];
        $expected = Carbon::parse(strval($rawValue));

        self::assertEquals($expected, $object->datelastmn);
    }

    public function testInvalidDate(): void
    {
        $object = self::$session
            ->query('icpricp')
            ->eq('itemno', 'MISTEST2')
            ->eq('pricelist', 'STAND')
            ->eq('dpricetype', 2)
            ->first()
        ;

        self::assertNotNull($object);
        self::assertNull($object->salestart);
    }
}
