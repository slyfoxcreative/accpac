<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Accpac\Session;

use function SlyFoxCreative\Utilities\assert_array;
use function SlyFoxCreative\Utilities\assert_instance_of;
use function SlyFoxCreative\Utilities\assert_int;
use function SlyFoxCreative\Utilities\assert_string;

class SandboxProductsTest extends TestCase
{
    private static Session $session;

    /** @var Collection<int, TestProduct> */
    private static Collection $products;

    /** @var Collection<string, Collection<int, string>> */
    private static Collection $kits;

    public static function setUpBeforeClass(): void
    {
        self::$session = new Session(
            $_ENV['ACCPAC_HOSTNAME'],
            $_ENV['ACCPAC_USERNAME'],
            $_ENV['ACCPAC_PASSWORD'],
            $_ENV['ACCPAC_DATABASE'],
            ['icitem', 'icitemo', 'ickith', 'icpric', 'icpricp', 'iciloc', 'icioth'],
        );

        $lines = file('./doc/test-products.md');

        if ($lines === false) {
            throw new \Exception('Failed to read ./doc/test-products.md');
        }

        $lines = collect($lines)->mapInto(Stringable::class);

        $productLines = $lines->takeUntil(fn($l) => $l->startsWith('## Kits'));
        $kitLines = $lines->skipUntil(fn($l) => $l->startsWith('## Kits'))->slice(1);

        $products = $productLines
            ->filter->startsWith('|')
            ->reject->startsWith('| Name')
            ->reject->startsWith('| -')
            ->chunk(26)
            ->map(function ($chunk) {
                return $chunk->mapWithKeys(function ($line) {
                    $parts = $line
                        ->trim("| \n")
                        ->explode('|')
                        ->mapInto(Stringable::class)
                        ->map->trim()
                    ;

                    return [(string) $parts[0] => (string) $parts[1]];
                })->toArray();
            })
        ;
        self::verifyProducts($products);
        self::$products = $products;

        $kits = $kitLines
            ->filter->startsWith('|')
            ->reject->startsWith('| Item')
            ->reject->startsWith('| -')
            ->map(function ($line) {
                return $line
                    ->trim("| \n")
                    ->explode('|')
                    ->mapInto(Stringable::class)
                    ->map->trim()
                    ->map(fn($s) => (string) $s)
                ;
            })
            ->mapToGroups(fn($c) => [$c[0] => $c[1]])
        ;
        self::verifyKits($kits);
        self::$kits = $kits;
    }

    /** @phpstan-assert Collection<int, TestProduct> $collection */
    public static function verifyProducts(mixed $collection): void
    {
        $keys = [
            'Item Number',
            'Description',
            'Price List',
            'Price',
            'Discount Level 1',
            'Discount Level 2',
            'Discount Level 3',
            'Discount Level 4',
            'Discount Level 5',
            'Special Price',
            'Special Start',
            'Special End',
            'Quantity On Hand',
            'Quantity Committed',
            'Quantity On Sales Order',
            'Quantity Shipped Not Costed',
            'Quantity Received Not Costed',
            'Quantity Adjusted Not Costed',
            'GTIN',
            'Weight',
            'Height',
            'Length',
            'Width',
            'Drop Ship',
            'Freight Class',
            'NMFC',
        ];
        assert_instance_of($collection, Collection::class);
        foreach ($collection as $index => $value) {
            assert_int($index);
            assert_array($value);
            foreach ($keys as $key) {
                assert_string($value[$key]);
            }
        }
    }

    /** @phpstan-assert Collection<string, TestKit> $collection */
    private static function verifyKits(mixed $collection): void
    {
        assert_instance_of($collection, Collection::class);
        foreach ($collection as $key => $kits) {
            assert_string($key);
            assert_instance_of($kits, Collection::class);
            foreach ($kits as $index => $value) {
                assert_int($index);
                assert_string($value);
            }
        }
    }

    public function testItem(): void
    {
        self::$products->each(function ($product) {
            $data = self::$session
                ->query('icitem')
                ->eq('itemno', $product['Item Number'])
                ->first()
            ;

            self::assertNotNull($data, $product['Item Number']);
            self::assertSame($product['Description'], $data->desc, $product['Item Number']);
            self::assertSame(floatval($product['Weight']), $data->unitwgt, $product['Item Number']);
        });
    }

    public function testPrice(): void
    {
        self::$products->each(function ($product) {
            $data = self::$session->query('icpricp')
                ->eq('itemno', $product['Item Number'])
                ->eq('pricelist', $product['Price List'])
                ->eq('dpricetype', 1)
                ->first()
            ;

            if ($product['Price'] === '—') {
                self::assertNull($data, $product['Item Number']);
            } else {
                self::assertNotNull($data, $product['Item Number']);
                self::assertSame(floatval($product['Price']), $data->unitprice, $product['Item Number']);
            }
        });
    }

    public function testDiscount(): void
    {
        self::$products->each(function ($product) {
            $data = self::$session->query('icpric')
                ->eq('itemno', $product['Item Number'])
                ->eq('pricelist', $product['Price List'])
                ->first()
            ;

            foreach (range(1, 5) as $level) {
                $message = "{$product['Item Number']} / {$level}";

                $key = "Discount Level {$level}";
                $property = "prcntlvl{$level}";

                if ($product[$key] === '—') {
                    self::assertNull($data, $message);
                } else {
                    self::assertNotNull($data, $message);
                    // @phpstan-ignore property.dynamicName
                    self::assertSame(floatval($product[$key]), $data->{$property}, $message);
                }
            }
        });
    }

    public function testSpecialPrice(): void
    {
        self::$products->each(function ($product) {
            $data = self::$session->query('icpricp')
                ->eq('itemno', $product['Item Number'])
                ->eq('pricelist', $product['Price List'])
                ->eq('dpricetype', 2)
                ->first()
            ;

            if ($product['Special Price'] === '—') {
                self::assertNull($data, $product['Item Number']);
            } else {
                self::assertNotNull($data, $product['Item Number']);
                self::assertSame(floatval($product['Special Price']), $data->unitprice, $product['Item Number']);

                $expected = $product['Special Start'] === '0' ? null : Carbon::parse(strval($product['Special Start']));
                self::assertEquals($expected, $data->salestart, $product['Item Number']);

                $expected = $product['Special End'] === '0' ? null : Carbon::parse(strval($product['Special End']));
                self::assertEquals($expected, $data->saleend, $product['Item Number']);
            }
        });
    }

    public function testInventory(): void
    {
        self::$products->each(function ($product) {
            $data = self::$session->query('iciloc')
                ->eq('itemno', $product['Item Number'])
                ->eq('location', '1')
                ->first()
            ;

            if ($product['Quantity On Hand'] === '—') {
                self::assertNull($data, $product['Item Number']);
            } else {
                self::assertNotNull($data);
                self::assertSame(intval($product['Quantity On Hand']), $data->qtyonhand, $product['Item Number']);
                self::assertSame(intval($product['Quantity Committed']), $data->qtycommit, $product['Item Number']);
                self::assertSame(intval($product['Quantity On Sales Order']), $data->qtysalordr, $product['Item Number']);
                self::assertSame(intval($product['Quantity Shipped Not Costed']), $data->qtyshnocst, $product['Item Number']);
                self::assertSame(intval($product['Quantity Received Not Costed']), $data->qtyrenocst, $product['Item Number']);
                self::assertSame(intval($product['Quantity Adjusted Not Costed']), $data->qtyadnocst, $product['Item Number']);
            }
        });
    }

    public function testGtin(): void
    {
        self::$products->each(function ($product) {
            $data = self::$session
                ->query('icioth')
                ->eq('itemno', $product['Item Number'])
                ->get()
                ->filter(fn($o) => preg_match('/^[0-9]{12}$/', $o->manitemno) === 1)
                ->first()
            ;

            if ($product['GTIN'] === '—') {
                self::assertNull($data, $product['Item Number']);
            } else {
                self::assertNotNull($data, $product['Item Number']);
                self::assertSame($product['GTIN'], $data->manitemno, $product['Item Number']);
            }
        });
    }

    public function testOptionalFields(): void
    {
        self::$products->each(function ($product) {
            $data = self::$session
                ->query('icitemo')
                ->eq('itemno', $product['Item Number'])
                ->get()
            ;

            if ($product['Height'] === '—') {
                self::assertFalse($data->contains(fn($d) => $d->optfield === 'HEIGHT'), $product['Item Number']);
            } else {
                $datum = $data->first(fn($d) => $d->optfield === 'HEIGHT');
                self::assertNotNull($datum, $product['Item Number']);
                self::assertSame($product['Height'], $datum->value, $product['Item Number']);
            }

            if ($product['Length'] === '—') {
                self::assertFalse($data->contains(fn($d) => $d->optfield === 'LENGTH'), $product['Item Number']);
            } else {
                $datum = $data->first(fn($d) => $d->optfield === 'LENGTH');
                self::assertNotNull($datum, $product['Item Number']);
                self::assertSame($product['Length'], $datum->value, $product['Item Number']);
            }

            if ($product['Width'] === '—') {
                self::assertFalse($data->contains(fn($d) => $d->optfield === 'WIDTH'), $product['Item Number']);
            } else {
                $datum = $data->first(fn($d) => $d->optfield === 'WIDTH');
                self::assertNotNull($datum, $product['Item Number']);
                self::assertSame($product['Width'], $datum->value, $product['Item Number']);
            }

            if ($product['Drop Ship'] === '—') {
                self::assertFalse($data->contains(fn($d) => $d->optfield === 'DROP'), $product['Item Number']);
            } else {
                $datum = $data->first(fn($d) => $d->optfield === 'DROP');
                self::assertNotNull($datum, $product['Item Number']);
                self::assertSame($product['Drop Ship'], $datum->value, $product['Item Number']);
            }

            if ($product['Freight Class'] === '—') {
                self::assertFalse($data->contains(fn($d) => $d->optfield === 'CLASS'), $product['Item Number']);
            } else {
                $datum = $data->first(fn($d) => $d->optfield === 'CLASS');
                self::assertNotNull($datum, $product['Item Number']);
                self::assertSame($product['Freight Class'], $datum->value, $product['Item Number']);
            }

            if ($product['NMFC'] === '—') {
                self::assertFalse($data->contains(fn($d) => $d->optfield === 'NMFC'), $product['Item Number']);
            } else {
                $datum = $data->first(fn($d) => $d->optfield === 'NMFC');
                self::assertNotNull($datum, $product['Item Number']);
                self::assertSame($product['NMFC'], $datum->value, $product['Item Number']);
            }
        });
    }

    public function testKits(): void
    {
        self::$kits->each(function ($expectedValues, $itemNumber) {
            $items = self::$session
                ->query('ickith')
                ->eq('itemno', $itemNumber)
                ->get()
            ;

            /** @var Collection<int, string> */
            $kitNumbers = $items->map->kitno->sort();

            foreach (collect($expectedValues)->zip($kitNumbers) as [$expected, $kitNumber]) {
                self::assertSame($expected, $kitNumber, "{$itemNumber} / {$expected}");
            }
        });
    }
}
