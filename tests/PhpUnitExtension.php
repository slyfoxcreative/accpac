<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use Dotenv\Dotenv;
use NunoMaduro\Collision\Adapters\Phpunit\Subscribers\EnsurePrinterIsRegisteredSubscriber;
use PHPUnit\Runner\Extension\Extension;
use PHPUnit\Runner\Extension\Facade;
use PHPUnit\Runner\Extension\ParameterCollection;
use PHPUnit\TextUI\Configuration\Configuration as PHPUnitConfiguration;

final class PhpUnitExtension implements Extension
{
    public function bootstrap(
        PHPUnitConfiguration $configuration,
        Facade $facade,
        ParameterCollection $parameters,
    ): void {
        // Read environment variables from .env
        if (is_readable(__DIR__ . '/../.env')) {
            Dotenv::createImmutable(__DIR__, '/../.env')->load();
        }

        // Disable default output
        $facade->replaceOutput();
        $facade->replaceProgressOutput();
        $facade->replaceResultOutput();

        // Enable Collision output
        $_SERVER['COLLISION_PRINTER'] = true;
        EnsurePrinterIsRegisteredSubscriber::register();
    }
}
