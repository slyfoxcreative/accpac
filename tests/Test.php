<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Accpac\FieldNotFound;
use SlyFoxCreative\Accpac\Session;
use SlyFoxCreative\Accpac\TableNotFound;

class Test extends TestCase
{
    private static Session $session;

    public static function setUpBeforeClass(): void
    {
        self::$session = new Session(
            $_ENV['ACCPAC_HOSTNAME'],
            $_ENV['ACCPAC_USERNAME'],
            $_ENV['ACCPAC_PASSWORD'],
            $_ENV['ACCPAC_DATABASE'],
            ['icitem'],
        );
    }

    public function testGettingFieldValue(): void
    {
        $object = self::$session
            ->query('icitem')
            ->eq('itemno', 'MISTEST1')
            ->first()
        ;

        self::assertNotNull($object);
        self::assertEquals('MISTEST1', $object->itemno);
    }

    public function testGettingNumericFieldValue(): void
    {
        $object = self::$session
            ->query('icitem')
            ->eq('itemno', 'MISTEST1')
            ->first()
        ;

        self::assertNotNull($object);
        self::assertEquals('0', $object->inactive);
    }

    public function testGettingInvalidFieldValue(): void
    {
        self::expectException(FieldNotFound::class);

        $object = self::$session
            ->query('icitem')
            ->eq('itemno', 'MISTEST1')
            ->first()
        ;

        // PHPStan complains that this line doesn't do anything, but but that's
        // OK because we just want to test that it triggers an exception.
        // @phpstan-ignore-next-line
        $object->invalid;
    }

    public function testInvalidTable(): void
    {
        self::expectException(TableNotFound::class);

        self::$session
            ->query('invalid')
            ->eq('foo', 'bar')
            ->first()
        ;
    }

    public function testInvalidField(): void
    {
        self::expectException(FieldNotFound::class);

        self::$session
            ->query('icitem')
            ->eq('invalid', 'MISTEST1')
            ->first()
        ;
    }

    public function testIsset(): void
    {
        $object = self::$session
            ->query('icitem')
            ->eq('itemno', 'MISTEST1')
            ->first()
        ;

        self::assertTrue(isset($object->itemno));
    }

    public function testEncoding(): void
    {
        mb_detect_order(['ASCII', 'UTF-8', 'ISO-8859-1']);

        $object = self::$session
            ->query('icitem')
            ->eq('itemno', 'MISTEST1')
            ->first()
        ;

        self::assertNotNull($object);
        self::assertSame('UTF-8', mb_detect_encoding($object->desc));
        self::assertSame('DO NOT USE (For testing only)À', $object->desc);
    }

    public function testEncodingWithQuery(): void
    {
        mb_detect_order(['ASCII', 'UTF-8', 'ISO-8859-1']);

        $value = self::$session->fetchValue("SELECT [desc] FROM icitem WHERE itemno = 'MISTEST1'");

        self::assertSame('UTF-8', mb_detect_encoding((string) $value));
        self::assertSame('DO NOT USE (For testing only)À                              ', $value);
    }

    public function testFetchRowWithNoResult(): void
    {
        $result = self::$session->fetchRow("SELECT * FROM icitem WHERE itemno = 'INVALID'");

        self::assertNull($result);
    }

    public function testFetchValueWithNoResult(): void
    {
        $result = self::$session->fetchRow("SELECT itemno FROM icitem WHERE itemno = 'INVALID'");

        self::assertNull($result);
    }

    public function testQueryBuilder(): void
    {
        $objects = self::$session
            ->query('icitem')
            ->select('itemno')
            ->like('itemno', 'MISTEST%')
            ->gt('unitwgt', 0.0)
            ->get()
        ;

        self::assertEquals(
            collect([
                collect(['ITEMNO' => 'MISTEST1']),
                collect(['ITEMNO' => 'MISTEST2']),
                collect(['ITEMNO' => 'MISTEST5-BLACK']),
                collect(['ITEMNO' => 'MISTEST5-KIT']),
                collect(['ITEMNO' => 'MISTEST5-WHITE']),
            ]),
            $objects->map->dump(),
        );
    }
}
