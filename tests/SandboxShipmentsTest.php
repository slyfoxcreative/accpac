<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Illuminate\Support\Stringable;
use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Accpac\Session;

use function SlyFoxCreative\Utilities\assert_array;
use function SlyFoxCreative\Utilities\assert_instance_of;
use function SlyFoxCreative\Utilities\assert_int;
use function SlyFoxCreative\Utilities\assert_string;

class SandboxShipmentsTest extends TestCase
{
    private static Session $session;

    /** @var Collection<string, TestShipment> */
    private static Collection $shipments;

    public static function setUpBeforeClass(): void
    {
        self::$session = new Session(
            $_ENV['ACCPAC_HOSTNAME'],
            $_ENV['ACCPAC_USERNAME'],
            $_ENV['ACCPAC_PASSWORD'],
            $_ENV['ACCPAC_DATABASE'],
            ['oeshih', 'oeshid'],
        );

        $lines = file('./doc/test-shipments.md');

        if ($lines === false) {
            throw new \Exception('Failed to read ./doc/test-shipments.md');
        }

        $lines = collect($lines)
            ->mapInto(Stringable::class)
            ->reject->startsWith("\n")
            ->reject->startsWith('# ')
            ->reject->startsWith('| -')
            ->reject->startsWith('| Shipment')
        ;

        $shipmentLines = $lines->takeUntil(fn($l) => $l->startsWith('## Items'));
        $itemLines = $lines->skipUntil(fn($l) => $l->startsWith('## Items'))->slice(1);

        /** @var Collection<string, Collection<int, array<string, mixed>>> */
        $items = new Collection();
        $itemLines->each(function ($line) use (&$items) {
            $cells = $line
                ->trim("| \n")
                ->explode('|')
                ->mapInto(Stringable::class)
                ->map->trim()
                ->map(fn($s) => (string) $s)
            ;

            assert_string($cells[0]);
            if (!$items->has($cells[0])) {
                $items[$cells[0]] = new Collection();
            }

            $items[$cells[0]]?->push([
                'item_number' => Str::remove('\\', (string) $cells[1]),
                'kit' => $cells[2],
                'quantity' => (int) $cells[3],
            ]);
        });

        $shipments = $shipmentLines->mapWithKeys(function ($line) use ($items) {
            $cells = $line
                ->trim("| \n")
                ->explode('|')
                ->mapInto(Stringable::class)
                ->map->trim()
                ->map(fn($s) => (string) $s)
            ;

            return [$cells[0] => [
                'carrier' => $cells[1],
                'tracking_number' => $cells[2] ?? '',
                'items' => ($items[$cells[0]] ?? new Collection())->toArray(),
            ]];
        });

        self::verifyShipments($shipments);
        self::$shipments = $shipments;
    }

    /** @phpstan-assert Collection<string, TestShipment> $collection */
    private static function verifyShipments(mixed $collection): void
    {
        assert_instance_of($collection, Collection::class);
        foreach ($collection as $key => $value) {
            assert_string($key);
            assert_array($value);
            assert_string($value['carrier']);
            assert_string($value['tracking_number']);
            assert_array($value['items']);
            foreach ($value['items'] as $index => $item) {
                assert_int($index);
                assert_string($item['item_number']);
                assert_string($item['kit']);
                assert_int($item['quantity']);
            }
        }
    }

    public function testShipments(): void
    {
        self::$shipments->each(function ($expected, $shipmentNumber) {
            $shipment = self::$session
                ->query('oeshih')
                ->eq('shinumber', $shipmentNumber)
                ->first()
            ;

            self::assertNotNull($shipment, $shipmentNumber);

            $items = self::$session
                ->query('oeshid')
                ->eq('shiuniq', $shipment->shiuniq)
                ->orderBy('item')
                ->get()
            ;

            self::assertTrue($items->isNotEmpty(), $shipmentNumber);

            self::assertSame($expected['carrier'], $shipment->shipvia, $shipmentNumber);
            self::assertSame($expected['tracking_number'], $shipment->shiptrack, $shipmentNumber);

            foreach (collect($expected['items'])->zip($items) as [$expectedItem, $item]) {
                $message = "{$shipmentNumber} / {$expectedItem['item_number']}";
                self::assertSame($expectedItem['item_number'], $item->item, $message);
                self::assertSame($expectedItem['kit'], $item->ddtlno, $message);
                self::assertSame($expectedItem['quantity'], $item->qtyshipped, $message);
            }
        });
    }
}
