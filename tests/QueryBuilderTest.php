<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Accpac\Direction;
use SlyFoxCreative\Accpac\QueryBuilder;
use SlyFoxCreative\Accpac\Session;

class QueryBuilderTest extends TestCase
{
    private static Session $session;

    public static function setUpBeforeClass(): void
    {
        self::$session = new Session(
            $_ENV['ACCPAC_HOSTNAME'],
            $_ENV['ACCPAC_USERNAME'],
            $_ENV['ACCPAC_PASSWORD'],
            $_ENV['ACCPAC_DATABASE'],
            ['icitem'],
        );
    }

    public function testBasicQuery(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');

        self::assertSame('SELECT * FROM ICITEM', $b->sql());
    }

    public function testEq(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->eq('itemno', 'MISTEST1');

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [ITEMNO] = :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 'MISTEST1']), $b->params());
    }

    public function testNotEq(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->eq('itemno', 'MISTEST1');

        self::assertSame(
            'SELECT * FROM ICITEM WHERE NOT [ITEMNO] = :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 'MISTEST1']), $b->params());
    }

    public function testNe(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->ne('itemno', 'MISTEST1');

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [ITEMNO] <> :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 'MISTEST1']), $b->params());
    }

    public function testNotNe(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->ne('itemno', 'MISTEST1');

        self::assertSame(
            'SELECT * FROM ICITEM WHERE NOT [ITEMNO] <> :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 'MISTEST1']), $b->params());
    }

    public function testIn(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->in('itemno', ['MISTEST1', 'MISTEST2', 'MISTEST3']);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [ITEMNO] IN (:p0, :p1, :p2)',
            $b->sql(),
        );
        self::assertEquals(
            collect([
                ':p0' => 'MISTEST1',
                ':p1' => 'MISTEST2',
                ':p2' => 'MISTEST3',
            ]),
            $b->params(),
        );
    }

    public function testNotIn(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->in('itemno', ['MISTEST1', 'MISTEST2', 'MISTEST3']);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE NOT [ITEMNO] IN (:p0, :p1, :p2)',
            $b->sql(),
        );
        self::assertEquals(
            collect([
                ':p0' => 'MISTEST1',
                ':p1' => 'MISTEST2',
                ':p2' => 'MISTEST3',
            ]),
            $b->params(),
        );
    }

    public function testInWithEmptyList(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->in('itemno', []);
        self::assertSame(
            'SELECT * FROM ICITEM WHERE [ITEMNO] <> [ITEMNO]',
            $b->sql(),
        );
    }

    public function testNotInWithEmptyList(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->in('itemno', []);
        self::assertSame(
            'SELECT * FROM ICITEM WHERE [ITEMNO] = [ITEMNO]',
            $b->sql(),
        );
    }

    public function testLike(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->like('itemno', 'MISTEST%');

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [ITEMNO] LIKE :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 'MISTEST%']), $b->params());
    }

    public function testNotLike(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->like('itemno', 'MISTEST%');

        self::assertSame(
            'SELECT * FROM ICITEM WHERE NOT [ITEMNO] LIKE :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 'MISTEST%']), $b->params());
    }

    public function testGt(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->gt('unitwgt', 1);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [UNITWGT] > :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 1]), $b->params());
    }

    public function testNotGt(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->gt('unitwgt', 1);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE NOT [UNITWGT] > :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 1]), $b->params());
    }

    public function testGe(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->ge('unitwgt', 1);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [UNITWGT] >= :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 1]), $b->params());
    }

    public function testNotGe(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->ge('unitwgt', 1);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE NOT [UNITWGT] >= :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 1]), $b->params());
    }

    public function testLt(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->lt('unitwgt', 1);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [UNITWGT] < :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 1]), $b->params());
    }

    public function testNotLt(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->lt('unitwgt', 1);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE NOT [UNITWGT] < :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 1]), $b->params());
    }

    public function testLe(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->le('unitwgt', 1);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [UNITWGT] <= :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 1]), $b->params());
    }

    public function testNotLe(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->le('unitwgt', 1);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE NOT [UNITWGT] <= :p0',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 1]), $b->params());
    }

    public function testNull(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->null('unitwgt');

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [UNITWGT] IS NULL',
            $b->sql(),
        );
    }

    public function testNotNull(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->not()->null('unitwgt');

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [UNITWGT] IS NOT NULL',
            $b->sql(),
        );
    }

    public function testOr(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->le('unitwgt', 1.0)->or()->ge('unitwgt', 5.0);

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [UNITWGT] <= :p0 OR [UNITWGT] >= :p1',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 1.0, ':p1' => 5.0]), $b->params());
    }

    public function testOrNot(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->eq('itemno', 'MISTEST1')->or()->not()->like('itemno', 'MISTEST%');

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [ITEMNO] = :p0 OR NOT [ITEMNO] LIKE :p1',
            $b->sql(),
        );
        self::assertEquals(collect([':p0' => 'MISTEST1', ':p1' => 'MISTEST%']), $b->params());
    }

    public function testParen(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b
            ->like('itemno', 'MISTEST%')
            ->or()->paren(fn($q) => $q->ge('unitwgt', 1.0)->le('unitwgt', 5.0))
        ;

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [ITEMNO] LIKE :p0 OR ([UNITWGT] >= :p1 AND [UNITWGT] <= :p2)',
            $b->sql(),
        );
        self::assertEquals(
            collect([
                ':p0' => 'MISTEST%',
                ':p1' => 1.0,
                ':p2' => 5.0,
            ]),
            $b->params(),
        );
    }

    public function testOrderBy(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->orderBy('itemno', Direction::Ascending);

        self::assertSame(
            'SELECT * FROM ICITEM ORDER BY [ITEMNO] ASC',
            $b->sql(),
        );
    }

    public function testPaginate(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->paginate(page: 2, perPage: 20);

        self::assertSame(
            'SELECT * FROM ICITEM OFFSET 20 ROWS FETCH NEXT 20 ROWS ONLY',
            $b->sql(),
        );
    }

    public function testMultipleConditions(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $b->eq('itemno', 'MISTEST1')
            ->ge('unitwgt', 2)
            ->le('unitwgt', 5)
            ->orderBy('unitwgt')
            ->paginate(perPage: 20)
        ;

        self::assertSame(
            'SELECT * FROM ICITEM WHERE [ITEMNO] = :p0 AND [UNITWGT] >= :p1 AND [UNITWGT] <= :p2 ORDER BY [UNITWGT] ASC OFFSET 0 ROWS FETCH NEXT 20 ROWS ONLY',
            $b->sql(),
        );
        self::assertEquals(
            collect([
                ':p0' => 'MISTEST1',
                ':p1' => 2,
                ':p2' => 5,
            ]),
            $b->params(),
        );
    }

    public function testGetBase(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $o = $b->select('itemno')->in('itemno', ['MISTEST1', 'MISTEST2'])->getBase();

        self::assertEquals(
            collect([
                collect(['ITEMNO' => 'MISTEST1']),
                collect(['ITEMNO' => 'MISTEST2']),
            ]),
            $o->map->dump(),
        );
    }

    public function testFirst(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $o = $b->select('itemno')->eq('itemno', 'MISTEST1')->first();

        self::assertNotNull($o);
        self::assertEquals(collect(['ITEMNO' => 'MISTEST1']), $o->dump());
    }

    public function testCount(): void
    {
        $b = new QueryBuilder(self::$session, 'icitem');
        $c = $b->select('itemno')->in('itemno', ['MISTEST1', 'MISTEST2'])->count();

        self::assertSame(2, $c);
    }
}
