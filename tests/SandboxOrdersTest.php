<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use Illuminate\Support\Collection;
use Illuminate\Support\Stringable;
use PHPUnit\Framework\TestCase;
use SlyFoxCreative\Accpac\Session;

use function SlyFoxCreative\Utilities\assert_array;
use function SlyFoxCreative\Utilities\assert_instance_of;
use function SlyFoxCreative\Utilities\assert_int;
use function SlyFoxCreative\Utilities\assert_string;

class SandboxOrdersTest extends TestCase
{
    private static Session $session;

    /** @var Collection<int, TestOrder> */
    private static Collection $orders;

    public static function setUpBeforeClass(): void
    {
        self::$session = new Session(
            $_ENV['ACCPAC_HOSTNAME'],
            $_ENV['ACCPAC_USERNAME'],
            $_ENV['ACCPAC_PASSWORD'],
            $_ENV['ACCPAC_DATABASE'],
            ['oeordh', 'oeordh1'],
        );

        $lines = file('./doc/test-orders.md');

        if ($lines === false) {
            throw new \Exception('Failed to read ./doc/test-orders.md');
        }

        $lines = collect($lines)->mapInto(Stringable::class);

        $orders = $lines
            ->filter->startsWith('|')
            ->reject->startsWith('| Name')
            ->reject->startsWith('| -')
            ->chunk(7)
            ->map(function ($chunk): array {
                return $chunk->mapWithKeys(function ($line) {
                    $parts = $line
                        ->trim("|\n")
                        ->explode('|')
                        ->mapInto(Stringable::class)
                        ->map->trim()
                    ;

                    return [(string) $parts[0] => (string) $parts[1]];
                })->toArray();
            })
        ;
        self::verifyOrders($orders);
        self::$orders = $orders;
    }

    /** @phpstan-assert Collection<int, TestOrder> $collection */
    private static function verifyOrders(mixed $collection): void
    {
        assert_instance_of($collection, Collection::class);
        foreach ($collection as $key => $value) {
            assert_int($key);
            assert_array($value);
            assert_string($value['Number']);
            assert_string($value['Tracking Number']);
            assert_string($value['Shipping Address 1']);
            assert_string($value['Shipping City']);
            assert_string($value['Shipping State']);
            assert_string($value['Shipping ZIP Code']);
            assert_string($value['Shipping Country']);
        }
    }

    public function testOrders(): void
    {
        self::$orders->each(function ($order) {
            $orderData = self::$session
                ->query('oeordh')
                ->eq('ordnumber', $order['Number'])
                ->first()
            ;

            self::assertNotNull($orderData, $order['Number']);

            $detailsData = self::$session
                ->query('oeordh1')
                ->eq('orduniq', $orderData->orduniq)
                ->first()
            ;

            self::assertNotNull($detailsData, $order['Number']);

            self::assertSame($order['Tracking Number'], $detailsData->shiptrack, $order['Number']);
            self::assertSame($order['Shipping Address 1'], $orderData->shpaddr1, $order['Number']);
            self::assertSame($order['Shipping City'], $orderData->shpcity, $order['Number']);
            self::assertSame($order['Shipping State'], $orderData->shpstate, $order['Number']);
            self::assertSame($order['Shipping ZIP Code'], $orderData->shpzip, $order['Number']);
            self::assertSame($order['Shipping Country'], $orderData->shpcountry, $order['Number']);
        });
    }
}
