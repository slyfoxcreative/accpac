<?php

declare(strict_types=1);

namespace SlyFoxCreative\Accpac\Tests;

use Orchestra\Testbench\TestCase as BaseTestCase;
use SlyFoxCreative\Accpac\ServiceProvider;

class TestCase extends BaseTestCase
{
    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
        ];
    }
}
